#include <stdio.h>
#include <stdlib.h>
#include "person.h"
#include "readfile.h"
#include "string.h"

int cmpperson(const void *a, const void * b){
    struct person *a1 = (struct person *) a;
    struct person *b1 = (struct person *) b;
    return a1->id - b1->id;
}
void printPerson(struct person * a){
     
     printf("%s ", a->firstname);
     printf("%s ", a->lastname);
     printf("%d ", a->salary);
     printf("%d\n",a->id);
     
}

int len_worker(struct person * worker){
    int len = 0;
    
    while(len < 64 &&  worker[len].id != 0){
        len ++;
    }
    return len;
}
int findById(struct person * worker, int len, int input){
    int left = 0, right = len - 1;
    while(left < right){
        int mid = (left + right) / 2;
        if( (worker[mid]).id == input ){
            return mid;
        } else if((worker[mid]).id < input){
            left = mid + 1;
        } else {
            right = mid - 1;
        }
        
    }
    if( (worker[left]).id == input )
        return left;
    return -1;
}
int read_file(char * file_name, struct person * worker){
     int len = len_worker(worker);
     if(open_file(file_name) < 0 ){
         printf("error with open file\n");
     }
     while(not_eof()){
          struct person * tmp = &worker[len];
          if(read_int( &(tmp->id)) < 0 || read_string(tmp->firstname) < 0 
             || read_string(tmp->lastname) < 0 || read_int(&(tmp->salary)) < 0 ) {
                 printf("error with parse person info");
                 return -1;
             }
          len ++;
     }
     close_file();
     qsort(worker, len, sizeof(struct person), cmpperson);
     return 0;
}
void print_database(struct person *worker){
    int len = len_worker(worker);
    for(int i =0; i < len; i++){
        printPerson(&worker[i]);
    }
    printf("Number of Employees (%d)\n", len);
    return;
}
int findByLastName(struct person * worker, int len, char * input){
    for(int i = 0; i < len; i ++){
        if(strcmp(input, worker[i].lastname) == 0){
            return i;
        }
    }
    return -1;

}
int addEmployee(struct person * worker, int len, struct person * input){
    worker[len] = *input;
    len ++;
    return 0;
}
