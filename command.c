#include <stdlib.h>
#include <stdio.h>
#include "person.h"
struct person data[1024];
int cmd_print_database(struct person *worker){
    printf("Employee DB\n");
    printf("----------------------------------\n");
    print_database(worker);
    printf("----------------------------------\n");
}
int cmd_findById(struct person * worker){
    int len = len_worker(worker);
    int id;
    printf("please input an id\n");
    if(scanf("%d", &id) < 0){
        printf("error with input id\n");
        return -1;
    }
    if(id < 100000 || id > 999999){
        printf("error with input range\n");
        return -1;
    }
    int res = findById(worker, len, id);
    if(res < 0){
        printf("Employee with id %d not found in DB\n", id);
    }else {
        printf("search result is:\n");
        printPerson(&worker[res]);
    }
    return 0;
}
int cmd_findByLastname(struct person * worker){
    int len = len_worker(worker);
    char str[64];
    printf("please input an lastname\n");
    if(scanf("%s", str) < 0){
        printf("error with input lastname\n");
        return -1;
    }
    
    int res = findByLastName(worker, len, str);
    if(res < 0){
        printf("Employee with lastname %s not found in DB\n", str);
    }else {
        printf("search result is:\n");
        printPerson(&worker[res]);
    }
    return 0;
}
int cmd_addEmployee(struct person * worker){
    struct person input;
    printf("please input %s:\n", "firstname");
    if(scanf("%s", input.firstname) < 0){
        printf("error with input firstname\n");
        return -1;
    }
    printf("please input %s:\n", "lastname");
    if(scanf("%s", input.lastname) < 0){
        printf("error with input lastname\n");
        return -1;
    }
    printf("please input %s:\n", "salary");
    if(scanf("%d", &(input.salary) ) < 0){
        printf("error with input salary\n");
        return -1;
    }
    printf("do you want to add the following employee to the DB?\n");
    printf("%s  %s    %d\n", input.firstname, input.lastname, input.salary);
    printf("Enter 1 for yes, 0 for no:\n");
    int flag;
    scanf("%d", &flag);
    if(flag == 0)
       return 0;
    int len = len_worker(worker);
    input.id = worker[len-1].id + 1;
    if(addEmployee(worker, len, &input) < 0){
        printf("error with add a new employee\n");
        return -1;
        
    }
    return 0;
}
void printMenu(){
  
    printf("Employee DB Menu:\n");
    printf("----------------------------------\n");
    printf("(1) Print the Database\n");
    printf("(2) Lookup by ID\n");
    printf("(3) Lookup by Last Name\n");
    printf("(4) Add an Employee\n");
    printf("(5) Quit\n");
    printf("----------------------------------\n");
}
int main(int argc, char ** argv){
    if( argc == 1) {
		printf("need a file\n");
		return 1;
	}
    char * file_name = argv[1];
    if(read_file(file_name, data) < 0){
        printf("error with init\n");
        return -1;
    }
    for(;;){
       int command;
       printMenu();
       printf("Enter your choice:\n");
       scanf("%d",&command);
       if(command == 5) {
           printf("good bye\n");
           break;
       }
         
       if(command < 1 || command > 5){
              printf("command not correct ! try again\n");
              continue;
        }
       switch(command){
           case 1 : cmd_print_database(data);
           break;
           case 2: cmd_findById(data);
           break;
           case 3: cmd_findByLastname(data);
           break;
           case 4: cmd_addEmployee(data);
       }

    }

    return 0;

}