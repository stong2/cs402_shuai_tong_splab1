#include <stdlib.h>
#include <stdio.h>

extern FILE *in;
extern int open_file(char *argc);
extern int read_int(int *input);
extern int read_string(char *input);
extern int read_float(float *input);
extern int close_file();
extern int not_eof();
