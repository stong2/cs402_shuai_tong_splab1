#define MAXNAME  64
struct person {
       char firstname[MAXNAME];
       char lastname[MAXNAME];
       int id;
       int salary;
};
extern int cmpperson(const void *a, const void * b);
extern void printPerson(struct person * a);
extern int len_worker(struct person * worker);
extern int findById(struct person * worker, int len, int input);
extern int read_file(char * file_name, struct person * worker);
extern void print_database(struct person *worker);
extern int findByLastName(struct person * worker, int len, char * input);
extern int addEmployee(struct person * worker, int len, struct person * input);