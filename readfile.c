#include <stdlib.h>
#include <stdio.h>
#include "readfile.h"
FILE *in;
int open_file(char *file_name) {
    in = fopen(file_name, "r");
    if(in == NULL){
        printf("err!\n");
        return -1;
    }
    return 0;
}
int read_int(int *input){
    int ret = fscanf(in, "%d", input);
    if(ret > 0)
       return 0;
    else
       return -1;
    
}
int read_string(char *input){
    
    int ret = fscanf(in, "%s", input);
    if(ret > 0)
       return 0;
    else
       return -1;
}
int read_float(float *input){
    int ret = fscanf(in, "%f", input);
    if(ret > 0)
       return 0;
    else
       return -1;
}
int close_file(){
    return fclose(in);
}
int not_eof(){
    return !feof(in);
}

